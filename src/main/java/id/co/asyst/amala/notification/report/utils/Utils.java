package id.co.asyst.amala.notification.report.utils;

import com.google.api.client.util.Value;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import id.co.asyst.commons.core.utils.DateUtils;
import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    private static Logger log = LogManager.getLogger(Utils.class);


    public void getQuery(Exchange exchange) {
        Map<String, Object> comparequery = (Map<String, Object>) exchange.getProperty("comparequery");

        String querymilesbq = (String) comparequery.get("query_total_miles_bq");
        String querymilesprod = (String) comparequery.get("query_total_miles_prod");
        String querytrxbq = (String) comparequery.get("query_total_trx_bq");
        String querytrxprod = (String) comparequery.get("query_total_trx_prod");

        exchange.setProperty("querymilesbq", querymilesbq);
        exchange.setProperty("querymilesprod", querymilesprod);
        exchange.setProperty("querytrxbq", querytrxbq);
        exchange.setProperty("querytrxprod", querytrxprod);
    }

    public void utilsProd(Exchange exchange) {
        Map<String, Object> resultmilesprod = (Map<String, Object>) exchange.getProperty("resultmilesprod");

        int tierprod = 0;
        int awardprod = 0;
        int frequencyprod = 0;

        if (resultmilesprod.get("total_tier") != null && !resultmilesprod.get("total_tier").toString().equals("0")){
            BigDecimal decimal = (BigDecimal) resultmilesprod.get("total_tier");
            tierprod = Integer.valueOf(decimal.intValue());
        }
        if (resultmilesprod.get("total_award") != null && !resultmilesprod.get("total_award").toString().equals("0")){
            BigDecimal decimal = (BigDecimal) resultmilesprod.get("total_award");
            awardprod = Integer.valueOf(decimal.intValue());
        }
        if (resultmilesprod.get("total_frequency") != null && !resultmilesprod.get("total_frequency").toString().equals("0")){
            BigDecimal decimal = (BigDecimal) resultmilesprod.get("total_frequency");
            frequencyprod = Integer.valueOf(decimal.intValue());
        }

        exchange.setProperty("tierprod", tierprod);
        exchange.setProperty("awardprod", awardprod);
        exchange.setProperty("frequencyprod", frequencyprod);
    }

    public void utilsBq(Exchange exchange) {
        HashMap<String, Object> resultquery = new HashMap<>();
        HashMap<String, Object> resultcountquery = new HashMap<>();
        String query = (String) exchange.getProperty("querymilesbq");
        String querycount = (String) exchange.getProperty("querytrxbq");
//        String path = (String) exchange.getProperty("path");
        String gcp = "/filestore/file-integration/report/amala-prd-4a58450b1f18.json";
//        String gcp = "D://AMALA/notification-report/gcp_keys/prod/amala-prd-4a58450b1f18.json";
        try {
            BigQuery bigquery = BigQueryOptions.newBuilder().setProjectId("amala-prd")
                    .setCredentials(
                            ServiceAccountCredentials.fromStream(new FileInputStream(gcp))
                    ).build().getService();
            QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();

            log.info("SELECT MILES BIGQUERY "+query);
            TableResult result = null;
            result = bigquery.query(queryConfig);
            for (FieldValueList row : result.iterateAll()) {
                resultquery = new HashMap<>();
                for (int i = 0; i < result.getSchema().getFields().size(); i++) {
                    if (row.get(i).getValue() != null) {
                        resultquery.put(result.getSchema().getFields().get(i).getName(), row.get(i).getValue());
                    } else {
                        resultquery.put(result.getSchema().getFields().get(i).getName(), 0);
                    }
                }
            }

            log.info("SELECT COUNT TRX BIGQUERY "+querycount);
            QueryJobConfiguration queryCountConfig = QueryJobConfiguration.newBuilder(querycount).build();
            TableResult resultcount = null;
            resultcount = bigquery.query(queryCountConfig);
            for (FieldValueList row : resultcount.iterateAll()) {
                resultcountquery = new HashMap<>();
                for (int i = 0; i < resultcount.getSchema().getFields().size(); i++) {
                    if (row.get(i).getValue() != null) {
                        resultcountquery.put(resultcount.getSchema().getFields().get(i).getName(), row.get(i).getValue());
                    } else {
                        resultcountquery.put(resultcount.getSchema().getFields().get(i).getName(), 0);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        exchange.setProperty("tierbq", resultquery.get("total_tier"));
        exchange.setProperty("awardbq", resultquery.get("total_award"));
        exchange.setProperty("frequencybq", resultquery.get("total_frequency"));
        exchange.setProperty("trxbq", resultcountquery.get("f0_"));

    }

    public void setUUID(Exchange exchange){
        UUID uuid = UUID.randomUUID();

        exchange.setProperty("uuid", uuid.toString());
    }

    public void createContentEmail(Exchange exchange) {
        List<Map<String, Object>> listReport = (List<Map<String, Object>>) exchange.getProperty("listReport");
        String bodyContent = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        for (Map<String, Object> report : listReport){
            String type = "";
            String tablebq = "";
            int report_id = (int) report.get("report_id");
            int trxbq = (int) report.get("total_trx_id_bq");
            int trxprod = (int) report.get("total_trx_id_prod");
            if (report_id == 1){
                type = "AIR ACCRUAL";
                tablebq = "air_accrual_report";
            }else if (report_id == 2){
                type = "AIR AWARD";
                tablebq = "air_award_report";
            }else if (report_id == 3){
                type = "BUY MILEAGE";
                tablebq = "buy_mile_age";
            }else if (report_id == 4){
                type = "NONAIR AWARD";
                tablebq = "non_air_award_report";
            }else if (report_id == 5){
                type = "NONAIR ACCRUAL";
                tablebq = "non_air_accrual_report";
            }else if (report_id == 6){
                type = "REDEPOSIT";
                tablebq = "redeposit_report";
            }else if (report_id == 7){
                type = "CUSTOM TRANSACTION";
                tablebq = "custom_transaction_report";
            }

            bodyContent = bodyContent.concat("<p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                    "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                    "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>" +
                    "Report "+type+" has been transfered into BigQuery </span><o:p></o:p></span></p>\n"+
                    "<p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                    "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                    "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>" +
                    "Detail data : </span><o:p></o:p></span></p>\n"+
                    "<p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                    "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                    "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>" +
                    "Transaction count = "+trxprod+"</span><o:p></o:p></span></p>\n"+
                    "<p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                    "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                    "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>" +
                    "Inserted report into "+tablebq+" = "+trxbq+"</span><o:p></o:p></span></p>\n" +
                    "<p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                    "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                    "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>" +
                    "Insert date "+format.format(date)+"</span><o:p></o:p></span></p>\n"+
                    "<table class=\"divider\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\" role=\"presentation\" valign=\"top\">\n" +
                    "                        <tbody>\n" +
                    "                          <tr style=\"vertical-align: top;\" valign=\"top\">\n" +
                    "                            <td class=\"divider_inner\" style=\"word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;\" valign=\"top\">\n" +
                    "                              <table class=\"divider_content\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid #BBBBBB; width: 100%;\" align=\"center\" role=\"presentation\" valign=\"top\">\n" +
                    "                                <tbody>\n" +
                    "                                  <tr style=\"vertical-align: top;\" valign=\"top\">\n" +
                    "                                    <td style=\"word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\" valign=\"top\"><span></span></td>\n" +
                    "                                  </tr>\n" +
                    "                                </tbody>\n" +
                    "                              </table>\n" +
                    "                            </td>\n" +
                    "                          </tr>\n" +
                    "                        </tbody>\n" +
                    "                      </table>\n");

        }
        String content = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n" +
                "\n" +
                "<head>\n" +
                "  <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->\n" +
                "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width\">\n" +
                "  <!--[if !mso]><!-->\n" +
                "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "  <!--<![endif]-->\n" +
                "  <title></title>\n" +
                "  <!--[if !mso]><!-->\n" +
                "  <!--<![endif]-->\n" +
                "  <style type=\"text/css\">\n" +
                "    body {\n" +
                "      margin: 0;\n" +
                "      padding: 0;\n" +
                "    }\n" +
                "\n" +
                "    table,\n" +
                "    td,\n" +
                "    tr {\n" +
                "      vertical-align: top;\n" +
                "      border-collapse: collapse;\n" +
                "    }\n" +
                "\n" +
                "    * {\n" +
                "      line-height: inherit;\n" +
                "    }\n" +
                "\n" +
                "    content[x-apple-data-detectors=true] {\n" +
                "      color: inherit !important;\n" +
                "      text-decoration: none !important;\n" +
                "    }\n" +
                "  </style>\n" +
                "  <style type=\"text/css\" id=\"media-query\">\n" +
                "    @media (max-width: 685px) {\n" +
                "\n" +
                "      .block-grid,\n" +
                "      .col {\n" +
                "        min-width: 320px !important;\n" +
                "        max-width: 100% !important;\n" +
                "        display: block !important;\n" +
                "      }\n" +
                "\n" +
                "      .block-grid {\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      .col {\n" +
                "        width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      .col_cont {\n" +
                "        margin: 0 auto;\n" +
                "      }\n" +
                "\n" +
                "      img.fullwidth,\n" +
                "      img.fullwidthOnMobile {\n" +
                "        max-width: 100% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col {\n" +
                "        min-width: 0 !important;\n" +
                "        display: table-cell !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack.two-up .col {\n" +
                "        width: 50% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num2 {\n" +
                "        width: 16.6% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num3 {\n" +
                "        width: 25% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num4 {\n" +
                "        width: 33% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num5 {\n" +
                "        width: 41.6% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num6 {\n" +
                "        width: 50% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num7 {\n" +
                "        width: 58.3% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num8 {\n" +
                "        width: 66.6% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num9 {\n" +
                "        width: 75% !important;\n" +
                "      }\n" +
                "\n" +
                "      .no-stack .col.num10 {\n" +
                "        width: 83.3% !important;\n" +
                "      }\n" +
                "\n" +
                "      .video-block {\n" +
                "        max-width: none !important;\n" +
                "      }\n" +
                "\n" +
                "      .mobile_hide {\n" +
                "        min-height: 0px;\n" +
                "        max-height: 0px;\n" +
                "        max-width: 0px;\n" +
                "        display: none;\n" +
                "        overflow: hidden;\n" +
                "        font-size: 0px;\n" +
                "      }\n" +
                "\n" +
                "      .desktop_hide {\n" +
                "        display: block !important;\n" +
                "        max-height: none !important;\n" +
                "      }\n" +
                "    }\n" +
                "  </style>\n" +
                "</head>\n" +
                "\n" +
                "<body class=\"clean-body\" style=\"margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: transparent;\">\n" +
                "  <!--[if IE]><div class=\"ie-browser\"><![endif]-->\n" +
                "  <table class=\"nl-container\" style=\"table-layout: fixed; vertical-align: top; min-width: 320px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: transparent; width: 100%;\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\" bgcolor=\"transparent\" valign=\"top\">\n" +
                "    <tbody>\n" +
                "      <tr style=\"vertical-align: top;\" valign=\"top\">\n" +
                "        <td style=\"word-break: break-word; vertical-align: top;\" valign=\"top\">\n" +
                "          <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" style=\"background-color:transparent\"><![endif]-->\n" +
                "          <div style=\"background-color:transparent;\">\n" +
                "            <div class=\"block-grid  no-stack\" style=\"min-width: 320px; max-width: 665px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;\">\n" +
                "              <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "                <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color:transparent;\"><tr><td align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:665px\"><tr class=\"layout-full-width\" style=\"background-color:transparent\"><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]><td align=\"center\" width=\"665\" style=\"background-color:transparent;width:665px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;\"><![endif]-->\n" +
                "                <div class=\"col num12\" style=\"min-width: 320px; max-width: 665px; display: table-cell; vertical-align: top; width: 665px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:10px; padding-bottom:10px; padding-right: 10px; padding-left: 10px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <div style=\"font-size:16px;text-align:center;font-family:Tahoma, Verdana, Segoe, sans-serif\">\n" +
                "                        <div class=\"our-class\" align=\"justify\">\n" +
                "                              <o:p></o:p>\n" +
                "                            </span></p>" +
                "\n" +
                "                          <p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                "6.0pt;margin-left:.3in;line-height:150%'><span class=SpellE><span style='font-size:12.0pt;line-height:150%;font-family:\"Tahoma\",sans-serif;\n" +
                "mso-fareast-font-family:\"Times New Roman\"'>INFORMASI REPORT</span></span>\n" +
                "\n" +
                "                          <p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>\n" +
                "                              <o:p>&nbsp;</o:p>\n" +
                "                            </span></p>\n" +
                "\n" +
                bodyContent +
                "                          <p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                "6.0pt;margin-left:.3in;line-height:150%'><span style='font-size:12.0pt;\n" +
                "line-height:150%;font-family:\"Tahoma\",sans-serif;mso-fareast-font-family:\"Times New Roman\"'>\n" +
                "                              <o:p>&nbsp;</o:p>\n" +
                "                            </span></p>\n" +
                "\n" +
                "                          <p class=MsoNormal style='margin-top:6.0pt;margin-right:.2in;margin-bottom:\n" +
                "6.0pt;margin-left:.3in;line-height:150%'>\n" +
                "                            <o:p>&nbsp;</o:p>\n" +
                "                          </p>\n" +
                "\n" +
                "                        </div>\n" +
                "                      </div>\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "              </div>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "          <div style=\"background-color:#0B72BA;\">\n" +
                "            <div class=\"block-grid mixed-two-up no-stack\" style=\"min-width: 320px; max-width: 665px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;\">\n" +
                "              <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "                <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color:#0B72BA;\"><tr><td align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:665px\"><tr class=\"layout-full-width\" style=\"background-color:transparent\"><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]><td align=\"center\" width=\"498\" style=\"background-color:transparent;width:498px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;\"><![endif]-->\n" +
                "                <div class=\"col num9\" style=\"display: table-cell; vertical-align: top; max-width: 320px; min-width: 495px; width: 498px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif\"><![endif]-->\n" +
                "                      <div style=\"color:#555555;font-family:Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\">\n" +
                "                        <div style=\"font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; line-height: 1.2; color: #555555; mso-line-height-alt: 14px;\">\n" +
                "                          <p style=\"font-size: 14px; line-height: 1.2; word-break: break-word; font-family: Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px; margin: 0;\"><br><br><br><span style=\"color: #ffffff; font-size: 14px;\">Garuda Indonesia is firmly committed to respect your privacy. We don't share your information with any third party without your consent. For more information please read the Garuda Indonesia <content style=\"text-decoration: underline; color: #ffffff;\" title=\"Privacy Policy.\" href=\"https://www.garuda-indonesia.com/id/en/contact/customer-care-policy/index.page?utm_source=mailchimp&utm_medium=email\" target=\"_blank\" rel=\"noopener\">Privacy Policy.</content></span></p>\n" +
                "                        </div>\n" +
                "                      </div>\n" +
                "                      <!--[if mso]></td></tr></table><![endif]-->\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td><td align=\"center\" width=\"166\" style=\"background-color:transparent;width:166px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;\"><![endif]-->\n" +
                "                <div class=\"col num3\" style=\"display: table-cell; vertical-align: top; max-width: 320px; min-width: 165px; width: 166px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <div class=\"img-container center  autowidth \" align=\"center\" style=\"padding-right: 0px;padding-left: 0px;\">\n" +
                "                        <!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr style=\"line-height:0px\"><td style=\"padding-right: 0px;padding-left: 0px;\" align=\"center\"><![endif]--><img class=\"center  autowidth \" align=\"center\" border=\"0\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70507.png\" alt=\"Image\" title=\"Image\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 160px; display: block;\" width=\"160\">\n" +
                "                        <!--[if mso]></td></tr></table><![endif]-->\n" +
                "                      </div>\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "              </div>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "          <div style=\"background-color:#0B72BA;\">\n" +
                "            <div class=\"block-grid two-up no-stack\" style=\"min-width: 320px; max-width: 665px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;\">\n" +
                "              <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "                <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color:#0B72BA;\"><tr><td align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:665px\"><tr class=\"layout-full-width\" style=\"background-color:transparent\"><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]><td align=\"center\" width=\"332\" style=\"background-color:transparent;width:332px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;\"><![endif]-->\n" +
                "                <div class=\"col num6\" style=\"display: table-cell; vertical-align: top; max-width: 320px; min-width: 330px; width: 332px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif\"><![endif]-->\n" +
                "                      <div style=\"color:#555555;font-family:Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\">\n" +
                "                        <div style=\"font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; line-height: 1.2; color: #555555; mso-line-height-alt: 14px;\">\n" +
                "                          <p style=\"font-size: 14px; line-height: 1.2; text-align: center; word-break: break-word; font-family: Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px; margin: 0;\"><span style=\"color: #ffffff; font-size: 14px;\">Download Our App</span></p>\n" +
                "                        </div>\n" +
                "                      </div>\n" +
                "                      <!--[if mso]></td></tr></table><![endif]-->\n" +
                "                      <table class=\"social_icons\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">\n" +
                "                        <tbody>\n" +
                "                          <tr style=\"vertical-align: top;\" valign=\"top\">\n" +
                "                            <td style=\"word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;\" valign=\"top\">\n" +
                "                              <table class=\"social_table\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;\" valign=\"top\">\n" +
                "                                <tbody>\n" +
                "                                  <tr style=\"vertical-align: top; display: inline-block; text-align: center;\" align=\"center\" valign=\"top\">\n" +
                "                                    <td style=\"word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 0;\" valign=\"top\"><content href=\"https://itunes.apple.com/id/app/garuda-indonesia-mobile/id961859722?mt=8&at=&ct=&ign-mpt=uo%3D6\" target=\"_blank\"><img width=\"32\" height=\"32\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70319.png\" alt=\"Apple\" title=\"Apple\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;\"></content></td>\n" +
                "                                    <td style=\"word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 0;\" valign=\"top\"><content href=\"https://play.google.com/store/apps/details?id=com.ursabyte.garudaindonesiaairlines&hl=en\" target=\"_blank\"><img width=\"32\" height=\"32\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70321.png\" alt=\"Android\" title=\"Android\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;\"></content></td>\n" +
                "                                  </tr>\n" +
                "                                </tbody>\n" +
                "                              </table>\n" +
                "                            </td>\n" +
                "                          </tr>\n" +
                "                        </tbody>\n" +
                "                      </table>\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td><td align=\"center\" width=\"332\" style=\"background-color:transparent;width:332px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;\"><![endif]-->\n" +
                "                <div class=\"col num6\" style=\"display: table-cell; vertical-align: top; max-width: 320px; min-width: 330px; width: 332px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma, Verdana, sans-serif\"><![endif]-->\n" +
                "                      <div style=\"color:#555555;font-family:Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\">\n" +
                "                        <div style=\"font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; line-height: 1.2; color: #555555; mso-line-height-alt: 14px;\">\n" +
                "                          <p style=\"font-size: 12px; line-height: 1.2; text-align: center; word-break: break-word; font-family: Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14px; margin: 0;\"><span style=\"color: #ffffff; font-size: 12px;\"><span style=\"font-size: 14px;\">Connect</span> With Us</span></p>\n" +
                "                        </div>\n" +
                "                      </div>\n" +
                "                      <!--[if mso]></td></tr></table><![endif]-->\n" +
                "                      <table class=\"social_icons\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">\n" +
                "                        <tbody>\n" +
                "                          <tr style=\"vertical-align: top;\" valign=\"top\">\n" +
                "                            <td style=\"word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;\" valign=\"top\">\n" +
                "                              <table class=\"social_table\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;\" valign=\"top\">\n" +
                "                                <tbody>\n" +
                "                                  <tr style=\"vertical-align: top; display: inline-block; text-align: center;\" align=\"center\" valign=\"top\">\n" +
                "                                    <td style=\"word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 0;\" valign=\"top\"><content href=\"https://www.facebook.com/garudaindonesia\" target=\"_blank\"><img width=\"32\" height=\"32\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70323.png\" alt=\"Facebook\" title=\"Facebook\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;\"></content></td>\n" +
                "                                    <td style=\"word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 0;\" valign=\"top\"><content href=\"https://twitter.com/indonesiagaruda\" target=\"_blank\"><img width=\"32\" height=\"32\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70325.png\" alt=\"Twitter\" title=\"Twitter\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;\"></content></td>\n" +
                "                                    <td style=\"word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 5px; padding-left: 0;\" valign=\"top\"><content href=\"https://www.youtube.com/user/garudaindonesia1949\" target=\"_blank\"><img width=\"32\" height=\"32\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70327.png\" alt=\"Youtube\" title=\"Youtube\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;\"></content></td>\n" +
                "                                  </tr>\n" +
                "                                </tbody>\n" +
                "                              </table>\n" +
                "                            </td>\n" +
                "                          </tr>\n" +
                "                        </tbody>\n" +
                "                      </table>\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "              </div>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "          <div style=\"background-color:#0B72BA;\">\n" +
                "            <div class=\"block-grid  no-stack\" style=\"min-width: 320px; max-width: 665px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;\">\n" +
                "              <div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\">\n" +
                "                <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color:#0B72BA;\"><tr><td align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:665px\"><tr class=\"layout-full-width\" style=\"background-color:transparent\"><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]><td align=\"center\" width=\"665\" style=\"background-color:transparent;width:665px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;\" valign=\"top\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;\"><![endif]-->\n" +
                "                <div class=\"col num12\" style=\"min-width: 320px; max-width: 665px; display: table-cell; vertical-align: top; width: 665px;\">\n" +
                "                  <div class=\"col_cont\" style=\"width:100% !important;\">\n" +
                "                    <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    <div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\">\n" +
                "                      <!--<![endif]-->\n" +
                "                      <div class=\"img-container center  autowidth \" align=\"center\" style=\"padding-right: 0px;padding-left: 0px;\">\n" +
                "                        <!--[if mso]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr style=\"line-height:0px\"><td style=\"padding-right: 0px;padding-left: 0px;\" align=\"center\"><![endif]--><img class=\"center  autowidth \" align=\"center\" border=\"0\" src=\"https://email-beeplugin-images.s3.amazonaws.com/images/insider-bee-client-garuda-production/70334.png\" alt=\"Image\" title=\"Image\" style=\"text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 150px; display: block;\" width=\"150\">\n" +
                "                        <!--[if mso]></td></tr></table><![endif]-->\n" +
                "                      </div>\n" +
                "                      <!--[if (!mso)&(!IE)]><!-->\n" +
                "                    </div>\n" +
                "                    <!--<![endif]-->\n" +
                "                  </div>\n" +
                "                </div>\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->\n" +
                "              </div>\n" +
                "            </div>\n" +
                "          </div>\n" +
                "          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "        </td>\n" +
                "      </tr>\n" +
                "    </tbody>\n" +
                "  </table>\n" +
                "  <!--[if (IE)]></div><![endif]-->\n" +
                "</body>\n" +
                "\n" +
                "</html>";

        exchange.setProperty("content", content);
    }


}
