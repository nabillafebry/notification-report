package id.co.asyst.amala.notification.report.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
