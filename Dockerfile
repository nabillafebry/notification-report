# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY /notification-report-1.2.0-SNAPSHOT.jar /notification-report-1.2.0-SNAPSHOT.jar
# run application with this command line[
CMD ["java", "-jar", "/notification-report-1.2.0-SNAPSHOT.jar"]
RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone